package com.zrj.service;

import com.zrj.pojo.TbUser;
import com.zrj.pojo.vo.User;
import org.springframework.stereotype.Service;

import java.util.List;

public interface UserService {
    List<TbUser> findAll();

    User login(String username,String password);

    void register(TbUser tbUser);
}
