package com.zrj.service;

import com.aliyuncs.exceptions.ClientException;
import com.zrj.pojo.vo.OssTokenVo;

public interface OssService {

  public OssTokenVo getToken() throws ClientException;
}
