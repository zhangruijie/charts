package com.zrj.service;

import com.zrj.pojo.TbChatRecord;

public interface ChartService {
    void insert(TbChatRecord chatRecord);

    void updateStatusHadRead(String id);
}
