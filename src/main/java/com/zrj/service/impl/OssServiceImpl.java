package com.zrj.service.impl;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.auth.sts.AssumeRoleRequest;
import com.aliyuncs.auth.sts.AssumeRoleResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.zrj.config.AliOssConfig;
import com.zrj.pojo.vo.OssTokenVo;
import com.zrj.service.OssService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OssServiceImpl implements OssService {

    @Autowired
    private AliOssConfig ossConfig;
    @Override
    public OssTokenVo getToken() throws ClientException {
        //构造请求器
        DefaultProfile profile = DefaultProfile.getProfile(
                ossConfig.getRegionID(), ossConfig.getAccessKeyId(), ossConfig.getAccessKeySecret());
        DefaultAcsClient acsClient = new DefaultAcsClient(profile);
        AssumeRoleRequest request = new AssumeRoleRequest();
        request.setRoleSessionName("zrj-session"); //随便写
        request.setRoleArn(ossConfig.getRoleArn());
        request.setDurationSeconds(1000L);
        AssumeRoleResponse acsResponse = acsClient.getAcsResponse(request);
        //拿到前端需要的数据
        AssumeRoleResponse.Credentials credentials = acsResponse.getCredentials();
        String accessKeyId = credentials.getAccessKeyId();
        String accessKeySecret = credentials.getAccessKeySecret();
        String securityToken = credentials.getSecurityToken();
        //构造前端需要的vo
        OssTokenVo ossTokenVo = new OssTokenVo();
        ossTokenVo.setAccessKeyId(accessKeyId);
        ossTokenVo.setAccessKeySecret(accessKeySecret);
        ossTokenVo.setStsToken(securityToken);
        ossTokenVo.setRegion("oss-"+ossConfig.getRegionID());
        ossTokenVo.setBucket(ossConfig.getBucket());
        return ossTokenVo;
    }
}
