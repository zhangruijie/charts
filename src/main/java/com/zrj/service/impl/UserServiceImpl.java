package com.zrj.service.impl;

import com.zrj.mapper.TbUserMapper;
import com.zrj.pojo.TbUser;
import com.zrj.pojo.TbUserExample;
import com.zrj.pojo.vo.User;
import com.zrj.service.UserService;
import com.zrj.util.IdWorker;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import javax.xml.ws.Action;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

   @Autowired
   private TbUserMapper userMapper;
   @Autowired
   private IdWorker idWorker;
    @Override
    public List<TbUser> findAll() {
        return userMapper.selectByExample(null);
    }

    @Override
    public User login(String username,String password) {
         if (StringUtils.isNotBlank(username) && StringUtils.isNotBlank(password)){
             TbUserExample userExample = new TbUserExample();
             TbUserExample.Criteria criteria = userExample.createCriteria();
             criteria.andUsernameEqualTo(username);
             List<TbUser> tbUsers = userMapper.selectByExample(userExample);
             if (tbUsers!=null && tbUsers.size()==1){
                 //进行密码校验，先做明文加密后在跟数据库密码做对比
                 String encodingPassword = DigestUtils.md5DigestAsHex(password.getBytes());
                 if (encodingPassword.equals(tbUsers.get(0).getPassword())){
                     User user=new User();
                     BeanUtils.copyProperties(tbUsers.get(0),user);
                     return user;
                 }
             }
         }
        return null;
    }

    @Override
    public void register(TbUser tbUser) {
        TbUserExample tbUserExample = new TbUserExample();
        TbUserExample.Criteria criteria = tbUserExample.createCriteria();
        criteria.andUsernameEqualTo(tbUser.getUsername());
        List<TbUser> tbUsers = userMapper.selectByExample(tbUserExample);
        if (tbUser!=null && tbUsers.size()>0){
            throw new RuntimeException("用户已经存在");
        }
        //使用雪花算法来生成唯一id
        tbUser.setId(idWorker.nextId());
        tbUser.setPassword(DigestUtils.md5DigestAsHex(tbUser.getPassword().getBytes()));
        tbUser.setPicSmall("");
        tbUser.setPicNormal("");
        tbUser.setNickname(tbUser.getUsername());
        tbUser.setQrcode("");
        tbUser.setCreatetime(new Date());
        userMapper.insert(tbUser);
    }

    public static void main(String[] args) {
        String password="1";
        String s = DigestUtils.md5DigestAsHex(password.getBytes());
        System.out.println(s);
    }
}
