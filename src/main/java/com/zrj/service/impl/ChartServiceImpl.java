package com.zrj.service.impl;

import com.zrj.mapper.TbChatRecordMapper;
import com.zrj.pojo.TbChatRecord;
import com.zrj.service.ChartService;
import com.zrj.util.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

public class ChartServiceImpl implements ChartService {

    @Autowired
    private TbChatRecordMapper chatRecordMapper;
    @Autowired
    private IdWorker idWorker;
    @Override
    public void insert(TbChatRecord chatRecord) {
        chatRecord.setId(idWorker.nextId());
        chatRecord.setCreatetime(new Date());
        chatRecord.setHasRead(0);
        chatRecord.setHasDelete(0);
        chatRecordMapper.insert(chatRecord);
    }

    @Override
    public void updateStatusHadRead(String id) {
        TbChatRecord tbChatRecord = chatRecordMapper.selectByPrimaryKey(id);
        tbChatRecord.setHasRead(1);
        chatRecordMapper.updateByPrimaryKey(tbChatRecord);
    }
}
