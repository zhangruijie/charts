package com.zrj.nettyChart;

import com.zrj.pojo.TbChatRecord;

/**
 * 消息实体
 */
public class Message {

    private Integer type; //消息类型  这里有前端定义，不同的类型处理不同的事情
    private TbChatRecord chatRecord;//聊天消息
    private Object ext; //扩展消息

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public TbChatRecord getChatRecord() {
        return chatRecord;
    }

    public void setChatRecord(TbChatRecord chatRecord) {
        this.chatRecord = chatRecord;
    }

    public Object getExt() {
        return ext;
    }

    public void setExt(Object ext) {
        this.ext = ext;
    }
}
