package com.zrj.nettyChart;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.springframework.stereotype.Component;

/**
 * Created by Zrj on
 *
 * @DATE: 2021/8/21
 * @TIME: 21:13
 */
@Component
public class WebSocketServer {

    private EventLoopGroup bossGroup;  //主线程池
    private EventLoopGroup workerGroup;//从线程池（工作线程池）
    private  ServerBootstrap server;//服务器
    private ChannelFuture future;//回调
    public void start(){
        future=server.bind(9001);
        System.out.println("netty server- 启动成功");
    }


    //初始化
    public WebSocketServer() {
           bossGroup = new NioEventLoopGroup();
           workerGroup = new NioEventLoopGroup();
               //创建netty服务器启动对象
               server = new ServerBootstrap();
               //初始化服务器启动对象
               server
                .group(bossGroup, workerGroup)
                //指定netty通道类型
                .channel(NioServerSocketChannel.class)
                //指定通道初始化器，用来加载当通道接收到消息后，如何处理消息
                .childHandler(new WebSocketChannlInnitializer());
    }
}
