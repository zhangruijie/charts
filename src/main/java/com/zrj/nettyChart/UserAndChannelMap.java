package com.zrj.nettyChart;

import io.netty.channel.Channel;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 建立用户和通道的关联
 */
public class UserAndChannelMap {

    private static Map<String, Channel> userAndChannelMap;

    static {
        userAndChannelMap=new HashMap<>();
    }

    /**
     * 用来添加用户id和通道的map对象
     * @param userId
     * @param channel
     */
    public static void put(String userId,Channel channel){
        userAndChannelMap.put(userId,channel);
    }

    /**
     * 根据用户id，移除用户和通道的关联关系
     * @param userId
     */
    public static void removeByUserId(String userId){
        userAndChannelMap.remove(userId);
    }

    /**
     * 根据通道id 移除用户和netty通道的关联关系
     * @param channelId
     */
    public static void removeByChannelId(String channelId){
        if (StringUtils.isBlank(channelId)){
            return;
        }
        for (String userId:userAndChannelMap.keySet()){
            Channel channel = userAndChannelMap.get(userId);
            if (channelId.equals(channel.id().asLongText())){
                userAndChannelMap.remove(userId);
            }
        }
    }

    /**
     * 根据用户id 查找通道
     * @param friendid
     * @return
     */
    public static Channel get(String friendid) {
        return userAndChannelMap.get(friendid);
    }
}
