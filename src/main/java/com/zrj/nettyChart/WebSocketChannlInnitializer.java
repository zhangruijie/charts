package com.zrj.nettyChart;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleStateHandler;

/**
 * Created by Zrj on
 *
 * @DATE: 2021/8/21
 * @TIME: 21:23
 */

/**
 * 通道初始化器 ，用来加载通道处理器（ChannelInitializer）
 */
public class WebSocketChannlInnitializer extends ChannelInitializer<SocketChannel> {


    /**
     * 这个通道接收到消息之后需要做什么事情
     * @param channel
     * @throws Exception
     */
    @Override
    protected void initChannel(SocketChannel channel) throws Exception {
         //获取管道，将一个个的channelHandler添加到管道中
        ChannelPipeline pipeline = channel.pipeline();

        //添加一个http的编解码器
        pipeline.addLast(new HttpServerCodec());
        //添加一个用于支持大数据流的支持
        pipeline.addLast(new ChunkedWriteHandler());
        //添加一个聚合器，这个聚合器主要是将HttpMessage聚合成 FullHttpRequest/response
        pipeline.addLast(new HttpObjectAggregator(1024*60));
        //需要指定接受请求的路由。必须使用以ws结尾的url才能访问
        pipeline.addLast(new WebSocketServerProtocolHandler("/ws"));

        //飞行模式下，netty不能够自动断开用户和通道连接，所以需要添加心跳机制
        //添加netty通道的空闲超时时间
        // 第一个参数 ：读空闲（超过一定时间会发送对应的事件消息）
        //第二个参数：写超时
        //第三个参数：读写超时
        pipeline.addLast(new IdleStateHandler(
                4,8,12));
        //添加心跳检查
        pipeline.addLast(new HearBeatHandler());
        //添加自定义handler
        pipeline.addLast(new ChartHandler());


    }
}
