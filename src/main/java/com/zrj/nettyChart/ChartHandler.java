package com.zrj.nettyChart;

import com.alibaba.fastjson.JSON;
import com.zrj.pojo.TbChatRecord;
import com.zrj.service.ChartService;
import com.zrj.util.IdWorker;
import com.zrj.util.SpringUtil;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.util.concurrent.EventExecutorGroup;
import io.netty.util.concurrent.GlobalEventExecutor;
import org.apache.commons.lang3.StringUtils;

import javax.jws.soap.SOAPBinding;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Zrj on
 *
 * @DATE: 2021/8/21
 * @TIME: 21:41
 */
public class ChartHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {

    //用来存放所有的客户端链接
    private static ChannelGroup clients=new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    /**
     * 管道接收到消息 的调用
     * @param cxt
     * @param msg
     * @throws Exception
     * 当客户端有新的消息进来，会自动调用
     */
    @Override
    protected void channelRead0(ChannelHandlerContext cxt, TextWebSocketFrame msg) throws Exception {
        //获取客户端发送来的消息
        String text = msg.text();
        System.out.println("接收到的数据消息为："+text);

        ChartService chartService=SpringUtil.getBean(ChartService.class);
        Message message = JSON.parseObject(text, Message.class);
        switch (message.getType()){
            case 0:           //建立用户的通道的关联
                String userid = message.getChatRecord().getUserid();
                Channel channel = cxt.channel();
                UserAndChannelMap.put(userid,channel);
                break;
            case 1:  //处理客户端发送好友消息
                //将聊天消息保存数据库
                TbChatRecord chatRecord = message.getChatRecord();
                chartService.insert(chatRecord);
                //获取好友的netty通道
                Channel channelFriend = UserAndChannelMap.get(chatRecord.getFriendid());
                if (channelFriend!=null){
                    //1.如果发送消息好友在线，那么直接发送消息给好友
                   channelFriend.writeAndFlush(new TextWebSocketFrame(JSON.toJSONString(message)));
                }else {
                    //2.如果发送消息的好友不在线，那么暂时不发送消息
                    System.out.println("用户的好友"+chatRecord.getFriendid()+"不在线");
                }
                break;
            case 2:   //处理客户端签收消息
                //将消息记录设置为已读
                chartService.updateStatusHadRead(message.getChatRecord().getId());
                break;
            case 3: //接受心跳消息
                System.out.println("接收到心跳消息："+JSON.toJSONString(message));
                break;
        }
    }

    /**
     * 当有客户端连接服务器后，会调用该方法
     */
    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        //将新的通道加入clients
        clients.add(ctx.channel());
    }

    /**
     * 当发生异常时，取消用户和netty通道的关联
     * @param ctx
     * @param cause
     * @throws Exception
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        UserAndChannelMap.removeByChannelId(ctx.channel().id().asLongText());
        ctx.channel().close();
    }

    /**
     * 当断开连接时，取消用户和netty通道的关联
     * @param ctx
     * @throws Exception
     */
    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        UserAndChannelMap.removeByChannelId(ctx.channel().id().asLongText());
        ctx.channel().close();
    }
}
