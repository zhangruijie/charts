package com.zrj.nettyChart;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.concurrent.EventExecutorGroup;

public class HearBeatHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent){
            IdleStateEvent idleStateEvent=(IdleStateEvent) evt;
            if (idleStateEvent.state()== IdleState.READER_IDLE){
                System.out.println("读空闲超时");
                //不做处理
            }else if (idleStateEvent.state()== IdleState.WRITER_IDLE){
                System.out.println("写空闲超时");
                //不做处理
            }else  if (idleStateEvent.state()== IdleState.ALL_IDLE){
                System.out.println("读写空闲超时");
                //不做处理，只是关闭通道
                ctx.channel().close();
            }
        }

    }
}
