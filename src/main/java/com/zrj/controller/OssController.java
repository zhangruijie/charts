package com.zrj.controller;

import com.aliyuncs.exceptions.ClientException;
import com.zrj.pojo.vo.OssTokenVo;
import com.zrj.service.OssService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/oss")
public class OssController {

    @Autowired
    private OssService ossService;

    /**
     * 获取token
     * 根据配置文件中的oss 参数信息，去服务器获取前端需要的token信息，
     * @return
     */
    @GetMapping("/getToken")
    public OssTokenVo getToken(){
        try {
            return ossService.getToken();
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return null;
    }
}
