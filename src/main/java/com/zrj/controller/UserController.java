package com.zrj.controller;

import com.zrj.pojo.TbUser;
import com.zrj.pojo.vo.Result;
import com.zrj.pojo.vo.User;
import com.zrj.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

  @Autowired
  private UserService userService;

  @RequestMapping("/findAll")
  @ResponseBody
  public List<TbUser> findAll(){
    return   userService.findAll();
  }

  @RequestMapping("/login")
  @ResponseBody
  public Result login(@RequestBody TbUser tbUser){
    try {
      User user=userService.login(tbUser.getUsername(),tbUser.getPassword());
      if (user==null){
        return new Result(false,"登录失败，请检查用户名或者密码");
      } else {
        return new Result(true,"登录成功",user);
      }
    } catch (Exception e) {
      e.printStackTrace();
      return new Result(false,"登录失败，请联系管理员");
    }
  }

  @RequestMapping("/register")
  public Result register(@RequestBody TbUser tbUser){
    try {
      userService.register(tbUser);
      return new Result(true,"注册成功");
    } catch (RuntimeException e) {
      e.printStackTrace();
      return new Result(false,e.getMessage());
    }
  }
}
