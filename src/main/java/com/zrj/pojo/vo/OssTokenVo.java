package com.zrj.pojo.vo;

/**
 * 前端需要的token信息
 */
public class OssTokenVo {
    // yourRegion填写Bucket所在地域。以华东1（杭州）为例，Region填写为oss-cn-hangzhou。
    private String region;
    // 从STS服务获取的临时访问密钥（AccessKey ID和AccessKey Secret）。
    private String accessKeyId;
    private String accessKeySecret;
    // 从STS服务获取的安全令牌（SecurityToken）。
    private String stsToken;
    // 填写Bucket名称。
    private String bucket;

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getAccessKeySecret() {
        return accessKeySecret;
    }

    public void setAccessKeySecret(String accessKeySecret) {
        this.accessKeySecret = accessKeySecret;
    }

    public String getStsToken() {
        return stsToken;
    }

    public void setStsToken(String stsToken) {
        this.stsToken = stsToken;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }
}
