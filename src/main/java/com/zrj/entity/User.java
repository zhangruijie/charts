package com.zrj.entity;

/**
 * Created by Zrj on
 *
 * @DATE: 2021/8/21
 * @TIME: 23:25
 */
public class User {
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
